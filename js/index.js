//hàm dùng để tính lương
function tinhLuong() {
  var soNgayLam = document.getElementById("input_so_ngaylam").value * 1;

  var soLuong = document.getElementById("input-nhapluong-1ngay").value * 1;
  var result = soLuong * soNgayLam;
  //   console.log("soNgayLam: ", soNgayLam);
  //   console.log("soLuong: ", soLuong);
  //   console.log("result: ", result);

  document.getElementById(
    "txtSalary"
  ).innerHTML = `<h1>Tổng lương của nhân viên:</h1> 
  <h2>Số tiền nhận được: ${result.toLocaleString()}VND</h2>
  <h2>Số ngày làm: ${soNgayLam}:</h2> `;
}

// hàm dùng để tính giá trị trung bình
function tinh_giatri_trungbinh() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  var num4 = document.getElementById("num4").value * 1;
  var num5 = document.getElementById("num5").value * 1;
  var resault = (num1 + num2 + num3 + num4 + num5) / 5;
  //   console.log("num1: ", num1);
  //   console.log("num2: ", num2);
  //   console.log("num3: ", num3);
  //   console.log("num4: ", num4);
  //   console.log("num5: ", num5);
  //   console.log("resault: ", resault);

  document.getElementById("txtTB").innerHTML = `Giá trị trung bình là: 
    <span class="display-4 text-warning"> ${resault}
    </span> `;
}
// hàm dùng để quy đổi USDC
function quydoiUSD() {
  var usd = document.getElementById("usd").value * 1;
  var quydoi = 23500 * usd;
  //   console.log("usd: ", usd);
  //   console.log("quydoi: ", quydoi);

  document.getElementById(
    "txtCurrency"
  ).innerHTML = `${quydoi.toLocaleString()} VND`;
}
// hàm tính chu vi -diện tích
function tinhChuVi_DienTich() {
  var chieuDai = document.getElementById("chieu-dai").value * 1;
  var chieuRong = document.getElementById("chieu-rong").value * 1;
  var tinhChuVi = (chieuDai + chieuRong) * 2;
  var tinhDientich = chieuDai * chieuRong;
  //   console.log('chieuDai: ', chieuDai);
  //   console.log('chieuRong: ', chieuRong);
  //   console.log('tinhChuVi: ', tinhChuVi);
  //   console.log('tinhDientich: ', tinhDientich);

  document.getElementById("txtCal").innerHTML = `Diện tích:${tinhDientich} 
    <br/>
    👉 Chu vi:${tinhChuVi} `;
}
// hàm tính tổng 2 ký số
function tinhTong2KySo() {
  var number = document.getElementById("number").value * 1;
  var hangDonVi = number % 10;
  var hangChuc = Math.floor(number / 10);
  var result = hangDonVi + hangChuc;
  //   console.log("hangDonVi: ", hangDonVi);
  //   console.log("hangChuc: ", hangChuc);
  //   console.log("number: ", number);
  //   console.log("result: ", result);
  document.getElementById("txtSum").innerHTML = `Tổng:${result}`;
}

// Goodbye. Thanks for the score love you.
